package br.com.senac_es.agenda;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.com.senac_es.agenda.adapter.AdapterAgenda;
import br.com.senac_es.agenda.dao.AgendaDao;
import br.com.senac_es.agenda.model.Agenda;
import br.com.senac_es.agenda.model.sobreAgenda;

public class MainActivity extends AppCompatActivity {


    public static final int REQUEST_NOVO = 1;
    public static final String AGENDA = "agenda";
    public static final String IMAGEM = "imagem";
    private static Bitmap imagem;


    public static Bitmap getImagem() {
        return imagem;
    }


    private ListView listView;
    private List<Agenda> lista = new ArrayList<>();
    private AdapterAgenda adapter;

    private Agenda agendaSelecionada;
    private AgendaDao dao;


    private void atualizarLista() {
        dao = new AgendaDao(this);
        lista = dao.pegarLista();
        dao.close();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        atualizarLista();

        listView = findViewById(R.id.agendalistview);

        adapter = new AdapterAgenda(this, lista);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View contexto, int posicao, long indice) {

                agendaSelecionada = (Agenda) adapter.getItemAtPosition(posicao);

                Intent intent = new Intent(MainActivity.this, MainApresentActivity.class);
                intent.putExtra(AGENDA, agendaSelecionada);

                startActivity(intent);


            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapter, View view, int posicao, long indice) {

                agendaSelecionada = (Agenda) adapter.getItemAtPosition(posicao);


                return false;
            }
        });


        registerForContextMenu(listView);


    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuItem menuItemLigar = menu.add("Ligar Telefone");

        menuItemLigar.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                Intent intent = new Intent(Intent.ACTION_CALL);
                Uri discar = Uri.parse("tel:" + agendaSelecionada.getTelefone());
                intent.setData(discar);

                startActivity(intent);


                return false;
            }
        });

        MenuItem menuItemLigarCell = menu.add("Ligar Celular");

        menuItemLigarCell.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                Intent intent = new Intent(Intent.ACTION_CALL);
                Uri discar = Uri.parse("tel:" + agendaSelecionada.getCelular());
                intent.setData(discar);
                startActivity(intent);


                return false;
            }
        });
    }


    public void novo(MenuItem item) {

        Intent intent = new Intent(this, AgendaCadActivity.class);
        startActivityForResult(intent, REQUEST_NOVO);


    }

    @Override
    protected void onResume() {
        super.onResume();
        atualizarLista();
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_NOVO) {

            switch (resultCode) {
                case RESULT_OK:
                    atualizarLista();
                    adapter.notifyDataSetChanged();
                    break;
                case RESULT_CANCELED:
                    Toast.makeText(this, "Cancelou", Toast.LENGTH_LONG).show();
                    break;

            }

        }


    }


    public void sobre(MenuItem item) {
        Intent intent = new Intent(this, sobreAgenda.class);
        startActivity(intent);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);

        return true;

    }


}







