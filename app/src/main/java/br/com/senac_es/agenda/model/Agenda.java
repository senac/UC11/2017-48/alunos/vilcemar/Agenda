package br.com.senac_es.agenda.model;


import java.io.Serializable;

public class Agenda implements Serializable {

    private int id;
    private String nome;
    private String endereco;
    private String telefone;
    private String celular;
    private String foto;


    public Agenda() {
    }

    public Agenda(int id, String nome, String endereco, String telefone, String celular, String foto) {
        this.id = id;
        this.nome = nome;
        this.endereco = endereco;
        this.telefone = telefone;
        this.celular = celular;
        this.foto = foto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

   @Override
    public String toString() {
        return "nome";
    }

}
