package br.com.senac_es.agenda;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.net.Uri;

import java.io.File;
import java.util.ArrayList;
import java.util.List;



import br.com.senac_es.agenda.dao.AgendaDao;
import br.com.senac_es.agenda.model.Agenda;

public class AgendaCadActivity extends AppCompatActivity {

    public static final int REQUEST_IMAGE_CAPTURE = 1;

    private ImageView imageView ;

    private EditText editNome;
    private EditText editelefote;
    private EditText editcelulcar;
    private EditText editentereco;

    private Button bntSalvar;
    private Button bnlLimpar;

    private String caminhoArquivo;


    private Agenda agenda;
    private AgendaDao agdao ;

        protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cad_agenda);

            imageView = findViewById(R.id.imageView);

            editNome = findViewById(R.id.editnome);
            editelefote = findViewById(R.id.editelefone);
            editcelulcar = findViewById(R.id.editcelular);
            editentereco = findViewById(R.id.editendereco);

            bntSalvar = findViewById(R.id.btnsalvar);
            bnlLimpar = findViewById(R.id.limpar);}


        public void capturarImagem(View view) {


            caminhoArquivo = Environment
                    .getExternalStorageDirectory().toString() + "/agenda/" + System.currentTimeMillis() + ".png" ;
          File arquivo = new File(caminhoArquivo) ;

            Uri localImagem = Uri.fromFile(arquivo) ;


            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            intent.putExtra (MediaStore. EXTRA_OUTPUT , localImagem) ;

            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
            }

        }

        @Override
        protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            ImageView imageView = findViewById(R.id.imageView);

            if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {

                agenda.setFoto(caminhoArquivo);

                Bitmap imagem = BitmapFactory.decodeFile(agenda.getFoto()) ;
                Bitmap imagemReduzida = Bitmap.createScaledBitmap(imagem , 200 , 200 , true);

               imageView.setImageBitmap(imagemReduzida);



            }


        }

        private boolean IsPreenchido(String valeu) {
            return (valeu != null && !valeu.isEmpty());
        }

        public void valiarDados() throws Exception {

            List<String> listaCamposRequeridos = new ArrayList<>();

            if (!IsPreenchido(agenda.getNome())) {
                listaCamposRequeridos.add("Nome");
                editNome.setBackgroundColor(getResources().getColor(R.color.vermelho));
            }

            if (!IsPreenchido(agenda.getTelefone())) {
                listaCamposRequeridos.add("Telefone");
            }

            if (!IsPreenchido(agenda.getCelular())) {
                listaCamposRequeridos.add("Celula");
            }

            if (!IsPreenchido(agenda.getEndereco())) {
                listaCamposRequeridos.add("Endereço");
            }

            if (listaCamposRequeridos.size() > 0) {
                throw new Exception("Campos requerido(s)" + listaCamposRequeridos.toString());
            }


        }


        public void salvar(View view) {


            try {

                String nome = editNome.getText().toString();
                String telefone = editelefote.getText().toString();
                String celula = editcelulcar.getText().toString();
                String endereco = editentereco.getText().toString();

                Bitmap foto = imageView.getDrawingCache();


                agenda = new Agenda();
                agenda.setNome(nome);
                agenda.setTelefone(telefone);
                agenda.setCelular(celula);
                agenda.setFoto(caminhoArquivo);
                agenda.setEndereco(endereco);


                this.valiarDados();

                agdao = new AgendaDao(this) ;
                agdao.salvar(agenda);
                agdao.close();

                setResult(RESULT_OK);

                finish();

            } catch (Exception ex) {

                AlertDialog.Builder builder = new AlertDialog.Builder(this) ;

                builder.setTitle("Erro de Validação")
                        .setMessage(ex.getMessage());

                AlertDialog dialog = builder.create() ;
                dialog.show();




            }


        }


}









