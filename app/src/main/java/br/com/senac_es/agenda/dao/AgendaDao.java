package br.com.senac_es.agenda.dao;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;


import br.com.senac_es.agenda.model.Agenda;

public class AgendaDao extends SQLiteOpenHelper {


    private static final int VERSAO = 1;
    private static final String DATABESE = "AgendaDb";

    public AgendaDao(Context context) {
        super(context, DATABESE, null, VERSAO);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String ddl = "CREATE TABLE IF NOT EXISTS agendadb ( " +
                "id INTEGER PRIMARY KEY, " +
                "nome TEXT NOT NULL," +
                "endereco TEXT, " +
                "telefone TEXT, " +
                "celular TEXT, " +
                "foto TEXT" +
                "); ";
        db.execSQL(ddl);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {

        String ddl = "DROP TABLE IF EXISTS agendadb ;";
        db.execSQL(ddl);
        this.onCreate(db);

    }

    public void salvar(Agenda agenda) {

        ContentValues valores = new ContentValues();
        valores.put("id", agenda.getId());
        valores.put("nome", agenda.getNome());
        valores.put("endereco", agenda.getEndereco());
        valores.put("telefone", agenda.getTelefone());
        valores.put("celular", agenda.getCelular());
        valores.put("foto", agenda.getFoto());

        getWritableDatabase().insert(
                "agendadb",
                null,
                valores);
    }

    public List<Agenda> pegarLista() {

        List<Agenda> lista = new ArrayList<>();
        String colunas[] = {"id", "nome", "endereco", "telefone", "celular", "foto"};

        Cursor direcao = getWritableDatabase().query(
                "agendadb", colunas, null, null, null, null, null
        );

        while (direcao.moveToNext()) {

            Agenda agenda = new Agenda();

            agenda.setId(direcao.getInt(0));
            agenda.setNome(direcao.getString(1));
            agenda.setEndereco(direcao.getString(2));
            agenda.setTelefone(direcao.getString(3));
            agenda.setCelular(direcao.getString(4));
            agenda.setFoto(direcao.getString(5));

            lista.add(agenda);
        }

        return lista ;
    }

}